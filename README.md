# Multi-Clustered Orchestration

The repositories in this group have been created to explore how we can take control of an environment with multiple Kubernetes clusters. Here we're able to provisioning cluster infrastructure automatically through Gitlab CI and have applications deployed to the cluster through ArgoCD and Gitops. 

See each repository for further details.

## Infrastructure

The infrastructure repository contains the Terraform code used to handle the following functionality:
 - Create/destroy cluster infrastructure
 - Deploy ArgoCD
 - Configure ArgoCD with application manifest repositories (deployed as ApplicationSets in Argo)
 - Register clusters to our ArgoCD instance

Combining this functionality we're able to automate the creation of new clusters and the deployment of applications to them.

## Application Manifest Repositories

Each application repository contains Helm charts to deploy our application. We can create as many manifest repositories as we want and attach them to ArgoCD.